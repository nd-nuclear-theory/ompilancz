#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

int main(int argc, char* argv[])
{
   std::cout << "Opening MFDn-stored file " << argv[1] << "..." << std::endl;
   std::ifstream mfdn_f(argv[1], std::ios::binary);

   mfdn_f.seekg(0, std::ios::end);
   long length = mfdn_f.tellg();
   mfdn_f.seekg(0, std::ios::beg);

   if (length % 4 != 0)
   {
      std::cerr << "File length is not divisible by 4." << std::endl;
      std::exit(1);
   }

   length /= 4;
   std::cout << "MFDn eigenvector length: " << length << std::endl; 

   std::vector<float> mfdn_v(length);

   for (long i = 0; i < length; i++)
      mfdn_f.read(reinterpret_cast<char*>(&mfdn_v[i]), sizeof(float));

   std::cout << "Opening OMPILancz-stored file " << argv[2] << "..." << std::endl;
   std::ifstream ompilancz_f(argv[2], std::ios::binary);

   ompilancz_f.seekg(0, std::ios::end);
   length = ompilancz_f.tellg();
   ompilancz_f.seekg(0, std::ios::beg);

   if (length % 4 != 0)
   {
      std::cerr << "File length is not divisible by 4." << std::endl;
      std::exit(1);
   }

   length /= 4;
   std::cout << "OMPILancz eigenvector length: " << length << std::endl; 

   std::vector<float> ompilancz_v(length);

   for (long i = 0; i < length; i++)
      ompilancz_f.read(reinterpret_cast<char*>(&ompilancz_v[i]), sizeof(float));

   double norm = 0.0;
   for (long i = 0; i < length; i++)
      norm += mfdn_v[i] * mfdn_v[i];
   norm = sqrt(norm);
   std::cout << "MFDn vector norm: " << norm << std::endl;

   norm = 0.0;
   for (long i = 0; i < length; i++)
      norm += ompilancz_v[i] * ompilancz_v[i];
   norm = sqrt(norm);
   std::cout << "OMPILancz vector norm: " << norm << std::endl;

   norm = 0.0;
   for (long i = 0; i < length; i++)
   {
      double temp = ompilancz_v[i] - mfdn_v[i];
      norm += temp * temp;
   }
   norm = sqrt(norm);
   std::cout << "Difference vector norm: " << norm << std::endl;

   double dot = 0.0;
   for (long i = 0; i < length; i++)
      dot += ompilancz_v[i] * mfdn_v[i];
   std::cout << "Dot product: " << dot << std::endl;

}
