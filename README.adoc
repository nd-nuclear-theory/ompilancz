= OMPILancz

Parallel Lanczos eigensolver written in {cpp} for hybrid MPI+OpenMP parallel programming model
