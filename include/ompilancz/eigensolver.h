#ifndef OMPILANCZ_EIGENSOLVER_H
#define OMPILANCZ_EIGENSOLVER_H

#include <mpi.h>

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>

#include "detail/block_diagonal_vector.h"
#include "detail/chrono_timer.h"
#include "detail/lapack_tridiagonal_eigensolver.h"
#include "detail/mapping.h"
#include "detail/matrix_times_vector.h"

namespace ompilancz
{

// single-vector Lanczos eigensolver:

template <typename T>
class eigensolver
{
   public:
      eigensolver(
            int I, int J, int N,    // process coordinates, number of diagonal processes
            uint64_t n, uint64_t m  // process-local matrix block dimensions
         )
         : map_(I, J, N, n, m), mdt_( mpi_datatype<T>::get() ), X_(map_)
      { }

      template <typename U>
      void solve(
            U&& matvec_operator,  // process-local matrix block x vector operator
            int nev,              // number of required eigenvalues
            int maxit,            // maximum number of iterations
            T eps)                // required residual accuracy
      {
         nev_ = nev;

         if (map_.root())
            std::cout << "Starting OMPILancz single-vector eigensolver..."
               << std::endl << std::endl;

         // open file for convergence logging

         if (map_.root())
            f_conv_log_.open("eigenvalues.dat");

         // reset timers
         matvec_time_ = 0.0;
         bcast_time_ = 0.0;
         local_matvec_time_ = 0.0;
         reduce_time_ = 0.0;
         reorthog_time_ = 0.0;
         tridiag_time_ = 0.0;

         chrono_timer solve_timer(chrono_timer::start_now);

         // setup data structure for Lanczos diagonal vectors:
         if (map_.diag())
         {
            v_ = std::make_unique< block_diagonal_vector<T> >(map_);

            // setup initial Lanczos vector:
            v_->add_random();
            v_->normalize_last();
         }

         // setup tridiagonal eigensolver on root:
         if (map_.root())
            tde_ = std::make_unique< tridiagonal_eigensolver<T> >(maxit);

         // setup matrix-vector multiplication wrapper:
         matrix_times_vector<T> matvec(map_);

         bool stop = false;  // stop flag

         // eigensolver iterations (indexed from 1):

         chrono_timer iterations_timer(chrono_timer::start_now);

         int j = 1;
         for ( ; j <= maxit; j++)
         {
            // additional Lanczos vector
            if (map_.diag())
               v_->add_zero();

            // u_j <- A * v_(j-1):

            chrono_timer matvec_timer(chrono_timer::start_now);

            if (map_.diag())
               matvec.diag_setup_xy(v_->ith_vector(j - 1), v_->ith_vector(j));

            chrono_timer bcast_timer(chrono_timer::start_now);
            matvec.broadcast_x();
            bcast_timer.stop();
            bcast_time_ += bcast_timer.seconds();

            chrono_timer local_matvec_timer(chrono_timer::start_now);
            matvec.multiply(matvec_operator);
            local_matvec_timer.stop();
            local_matvec_time_ += local_matvec_timer.seconds();

            chrono_timer reduce_timer(chrono_timer::start_now);
            matvec.reduce_y();
            reduce_timer.stop();
            reduce_time_ += reduce_timer.seconds();

            matvec_timer.stop();
            matvec_time_ += matvec_timer.seconds();

            // reorthogonalization:

            if (map_.diag())
            {
               chrono_timer reorthog_timer(chrono_timer::start_now);

               T alpha, beta;
               std::tie(alpha, beta) = v_->orthonormalize_last();

               reorthog_timer.stop();
               reorthog_time_ += reorthog_timer.seconds();

               // solve projected tridiagonal eigenproblem:

               if (map_.root())
               {
                  chrono_timer tridiag_timer(chrono_timer::start_now);

                  tde_->append_alphabeta(alpha, beta);
                  tde_->solve(nev);

                  tridiag_timer.stop();
                  tridiag_time_ += tridiag_timer.seconds();

                  // check convergence:

                  if (j >= nev)
                  {
                     stop = true;  // may stop

                     for (int i = 0; i < nev; i++)
                        if (tde_->res(i) > eps)
                           stop = false;
                  }

                  // print current 1st, 2nd, and nev-th eigenvalues:

                  std::cout << "It. " << std::right << std::setw(4) << j << " - Ev.: ";

                                               print_lambda_res(tde_->lambda(      0), tde_->res(      0));
                  if ((j > 1) && (nev > 1))    print_lambda_res(tde_->lambda(      1), tde_->res(      1));
                  if ((j >= nev) && (nev > 2)) print_lambda_res(tde_->lambda(nev - 1), tde_->res(nev - 1));

                  std::cout << std::endl;

                  // log convergence
                  f_conv_log_ << "It. " << std::right << std::setw(4) << j << "  ";
                  for (int i = 0; i < std::min(j, nev); i++)
                     log_lambda_res(tde_->lambda(i), tde_->res(i));
                  f_conv_log_ << "\n";
               }
            }
           
            // stop iterating if convergence has been reached:

            MPI_Bcast(&stop, 1, MPI_CXX_BOOL, 0, map_.allcomm());
            if (stop)
               break;
         }

         // last iteration index:
         assert(j <= maxit + 1);
         n_ = std::min(j, maxit);

         iterations_timer.stop();
         iterations_time_ = iterations_timer.seconds();

         // obtain resulting eigenpairs on diagonal processes:

         if (map_.diag())
         {
            assert(v_->size() == n_ + 1);
            v_->remove_last();  // not needed anymore
            assert(v_->size() == n_);

            chrono_timer reconstr_timer(chrono_timer::start_now);

            diags_get_eigenvalues();
            diags_get_eigenvectors();

            reconstr_timer.stop();
            reconstr_time_ = reconstr_timer.seconds();
         }

         solve_timer.stop();
         solve_time_ = solve_timer.seconds();

         if (map_.root())
            std::cout << std::endl << "...eigensolver finished." << std::endl;
      }

      // final residual norms ||A x_i - lambda_i x_i||_2 for i=1,...,nev
      template <typename U>
      void residuals(U&& matvec_operator)
      {
         chrono_timer residuals_timer(chrono_timer::start_now);

         std::unique_ptr< diagonal_vector<T> > r;
         if (map_.diag())
            r = std::make_unique< diagonal_vector<T> >(map_);

         matrix_times_vector<T> matvec(map_);

         if (map_.root())
            std::cout << std::endl;

         for (int i = 0; i < nev_; i++)
         {
            // r_i <- A * x_i :

            if (map_.diag())
               matvec.diag_setup_xy(X_[i], *r);
            matvec.broadcast_x();
            matvec.multiply(matvec_operator);
            matvec.reduce_y();

            if (!map_.diag())
               continue;

            // r_i <- r_i - lambda_i * x_i :
            r->scaled_subtract(Lambda_[i], X_[i]);

            // norm:
            T r_norm = r->norm();

            residuals_timer.stop();
            residuals_time_ = residuals_timer.seconds();

            if (map_.root())
               std::cout << "Ev. " << std::right << std::setw(4) << i + 1 << " value = "
                  << std::right << std::setw(14) << std::setprecision(8) << std::fixed << Lambda_[i]
                  << ", res = " << std::scientific << std::setprecision(3) << r_norm << std::endl;
         }
      }

      template <typename U = T>
      void store_eigenvectors(const std::string& fileprefix, const std::string& extension = "dat", int num_width = 3)
      {
         if (map_.diag())
            X_.template store_to_files<U>(fileprefix, extension, num_width);
      }

      void print_times()
      {
         double temp[3] = { bcast_time_, reduce_time_, local_matvec_time_ };
         if (map_.root())
         {
            MPI_Reduce(MPI_IN_PLACE, temp, 2, MPI_DOUBLE, MPI_MIN, 0, map_.allcomm());
            MPI_Reduce(MPI_IN_PLACE, temp + 2, 1, MPI_DOUBLE, MPI_MAX, 0, map_.allcomm());
         }
         else
         {
            MPI_Reduce(temp, nullptr, 2, MPI_DOUBLE, MPI_MIN, 0, map_.allcomm());
            MPI_Reduce(temp + 2, nullptr, 1, MPI_DOUBLE, MPI_MAX, 0, map_.allcomm());
         }

         if (map_.root())
         {
            std::cout << std::endl << "Root process:" << std::endl;
            std::cout << "  Solve time: ................................... "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << solve_time_ << " [s]" << std::endl;
            std::cout << "  Total iterations time: ........................ "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << iterations_time_ << " [s]" << std::endl;
            std::cout << "  Total matvec time: ............................ "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << matvec_time_ << " [s]" << std::endl;
            std::cout << "  Total reorthogonalization time: ............... "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << reorthog_time_ << " [s]" << std::endl;
            std::cout << "  Total tridiagonal solver time: ................ "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << tridiag_time_ << " [s]" << std::endl;
            std::cout << "  Eigenvectors reconstruction time: ............. "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << reconstr_time_ << " [s]" << std::endl;

            if (residuals_time_ > 0.0)
               std::cout << "  Residual evaluation time: ..................... "
                  << std::right << std::setw(10) << std::setprecision(2) << std::fixed << reconstr_time_ << " [s]" << std::endl;

            std::cout << "All processes:" << std::endl;
            std::cout << "  Total maximal local matvec time: .............. "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << temp[2] << " [s]" << std::endl;
            std::cout << "  Total minimal broadcast communicatoin time: ... "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << temp[0] << " [s]" << std::endl;
            std::cout << "  Total minimal reduction communicatoin time: ... "
               << std::right << std::setw(10) << std::setprecision(2) << std::fixed << temp[1] << " [s]" << std::endl;
         }
      }

   private:
      mapping map_;

      int nev_;

      int n_;

      std::unique_ptr< block_diagonal_vector<T> > v_;

      std::unique_ptr< tridiagonal_eigensolver<T> > tde_;

      std::vector<T> Lambda_; 
      block_diagonal_vector<T> X_;  // must be placed after map_

      double solve_time_;
      double iterations_time_;
      double matvec_time_;
      double bcast_time_;
      double local_matvec_time_;
      double reduce_time_;
      double reorthog_time_;
      double tridiag_time_;
      double reconstr_time_;
      double residuals_time_ = 0.0;

      MPI_Datatype mdt_;

      std::ofstream f_conv_log_; 

      // implementation member functions:

      void print_lambda_res(T lambda, T res)
      {
         std::cout
            << std::right << std::setw(8) << std::setprecision(4) << std::fixed << lambda
            << " (" << std::scientific << std::setprecision(3) << res << ")   ";
      }

      void log_lambda_res(T lambda, T res)
      {
         f_conv_log_
            << std::right << std::setw(8) << std::setprecision(4) << std::fixed << lambda
            << " (" << std::scientific << std::setprecision(3) << res << ")   ";
      }

      void diags_get_eigenvalues()
      {
         assert (map_.diag());

         // broadcase eigenvalues to diagonal processes:

         if (map_.root())
         {
            span<T> Lambda = tde_->Lambda();
            Lambda_.assign(Lambda.begin(), Lambda.end());

            assert(Lambda_.size() == nev_);
         }
         else
            Lambda_.resize(nev_);

         MPI_Bcast(Lambda_.data(), nev_, mdt_, 0, map_.dcomm());
      }

      void diags_get_eigenvectors()
      {
         assert (map_.diag());

         // broadcast projected eigenvectors to diagonal processes:

         std::vector<T> Y;
         if (map_.root())
         {
            span<T> Temp = tde_->X();
            Y.assign(Temp.begin(), Temp.end());

            assert( Y.size() == nev_ * n_ );
         }
         else
            Y.resize(nev_ * n_);

         MPI_Bcast(Y.data(), nev_ * n_, mdt_, 0, map_.dcomm());

         // X <- Lanczos vectors * Y:

         assert( v_->size() == n_ );

         X_ = v_->right_multiply_by_nxn(Y);

         assert( X_.size() == nev_ );
      }
};

}  // namespace ompilancz

#endif
