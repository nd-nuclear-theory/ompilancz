#ifndef OMPILANCZ_DETAIL_LAPACK_LAMCH_H
#define OMPILANCZ_DETAIL_LAPACK_LAMCH_H

// LAPACK routines interface:
extern "C"
{
   extern float slamch_(char*);
   extern double dlamch_(char*);
}

// helper to run ?lamch (cannot be overloaded based on return type only):

template <typename T>
struct Lamch;

template <>
struct Lamch<float>
{
   static float lamch(char CMACH)
   {
      return slamch_(&CMACH);
   }
};

template <>
struct Lamch<double>
{
   static double lamch(char CMACH)
   {
      return dlamch_(&CMACH);
   }
};

#endif
