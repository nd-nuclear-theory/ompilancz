#ifndef OMPILANCZ_DETAIL_LAPACK_BLOCK_TRIDIAGONAL_EIGENSOLVER_H
#define OMPILANCZ_DETAIL_LAPACK_BLOCK_TRIDIAGONAL_EIGENSOLVER_H

#include <algorithm>
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <vector>

#include "lapack_lamch.h"
#include "../span.h"

// LAPACK routines interface:
extern "C"
{
   extern void ssbevx_(char*, char*, char*, int*, int*, float*, int*, float*, int*,
         float*, float*, int*, int*, float*, int*, float*, float*, int*,
         float*, int*, int*, int*);
   extern void dsbevx_(char*, char*, char*, int*, int*, double*, int*, double*, int*,
         double*, double*, int*, int*, double*, int*, double*, double*, int*,
         double*, int*, int*, int*);
}

// overloaded LAPACK wrappers:
inline void sbevx(char JOBZ, char RANGE, char UPLO, int N, int KD, float* AB, int LDAB, float* Q, int LDQ,
      float VL, float VU, int IL, int IU, float ABSTOL, int& M, float* W, float* Z, int LDZ,
      float* WORK, int* IWORK, int* IFAIL, int& INFO)
{
   ssbevx_(&JOBZ, &RANGE, &UPLO, &N, &KD, AB, &LDAB, Q, &LDQ, &VL, &VU, &IL, &IU, &ABSTOL,
         &M, W, Z, &LDZ, WORK, IWORK, IFAIL, &INFO);
}

inline void sbevx(char JOBZ, char RANGE, char UPLO, int N, int KD, double* AB, int LDAB, double* Q, int LDQ,
      double VL, double VU, int IL, int IU, double ABSTOL, int& M, double* W, double* Z, int LDZ,
      double* WORK, int* IWORK, int* IFAIL, int& INFO)
{
   dsbevx_(&JOBZ, &RANGE, &UPLO, &N, &KD, AB, &LDAB, Q, &LDQ, &VL, &VU, &IL, &IU, &ABSTOL,
         &M, W, Z, &LDZ, WORK, IWORK, IFAIL, &INFO);
}

namespace ompilancz
{

// Lapack symmetric block tridiagonal eigensolver wrapper:

template <typename T>
class block_tridiagonal_eigensolver
{
   public:
      block_tridiagonal_eigensolver(int s, int N_est = 0)  // s - block size, Nest - estimated maximum number of blocks 
         : s_(s), S_(s * s), N_(0), n_(0), nev_(0)
      {
         // avoid unnecessary reallocations:
         Alpha_.reserve(N_est * S_);
         Beta_.reserve(N_est * S_);

         int n_est = N_est * s;

         W_.reserve(n_est);
         Z_.reserve(n_est * n_est);
         Q_.reserve(n_est * n_est);
         WORK_.reserve(7 * n_est);
         IWORK_.reserve(5 * n_est);
         IFAIL_.reserve(n_est);
      }

      // Alpha, Beta: 2-D column-major (s * s) arrays
      template <typename RAIt>
      void append_AlphaBeta(RAIt Alpha_it, RAIt Beta_it)
      {
         int offset = N_ * s_;  // next block elements index

         assert(Alpha_.size() == offset);
         assert(Beta_.size() == offset);

         Alpha_.resize(offset + S_);
         Beta_.resize(offset + S_);

         std::copy(Alpha_it, Alpha_it + S_, Alpha_.begin() + offset);
         std::copy(Beta_it, Beta_it + S_, Beta_.begin() + offset);
         
         N_++;
         n_ += s_;
      }

      void solve(int nev)  // nev - number of required eigenpairs
      {
         nev_ = nev;

         pack();  // transform blocks to Lapack band form

         int m = s_ + 1;

         // setup array sizes:
         W_.resize(n_);
         Z_.resize(n_ * nev);
         Q_.resize(n_ * n_);
         WORK_.resize(7 * n_);
         IWORK_.resize(5 * n_);
         IFAIL_.resize(n_);

         T ABSTOL = Lamch<T>::lamch('S');
         int M, INFO;

         sbevx('V', 'I', 'L', n_, m - 1, AB_.data(), m, Q_.data(), n_, (T)0.0, (T)0.0, 1, std::min(n_, nev),
               ABSTOL, M, W_.data(), Z_.data(), n_, WORK_.data(), IWORK_.data(), IFAIL_.data(), INFO);

         if (INFO)
         {
//          std::cerr << "Block tridiagonal eigensolver failed; ?sbevx INFO not null: " << INFO << std::endl;
            throw std::runtime_error("LAPACK ?sbevx failed");
         }
      }

      // eigenvalue array:
      const span<T> Lambda() const
      {
         return span<T>(W_.data(), nev_);
      }

      // i-th (0-indexed) eigenvalue:
      T lambda(int i) const
      {
         assert(i < nev_);

         return W_[i];
      }

      // eigenvectors (2-D column-major) array:
      const span<T> X() const
      {
         return span<T>(Z_.begin(), Z_.end());
      }

      // i-th (0-indexed) eigenvector:
      const span<T> x(int i) const
      {
         assert(i < nev_);

         T* begin = Z_.data() + i * n_;
         return span<T>(begin, begin + n_);
      }

      // Lanczos residual for i-th eigenpair...
      // ... calculated as norm( last Beta * last part of i-th eigenvector )
      T res(int i) // const : uses WORK_ to avoid allocations; see below
      {
         assert(i < nev_);

         const std::vector<T>& last_Beta = Beta_.back();

         // extract last s_ elements of i-th eigenvector:
         span<T> ith_x = x(i);
         T* begin = ith_x.data() + (N_ - 1) * s_;
         span<T> xs(begin, begin + s_); 

         // multiply last_Beta * xs:
//       std::vector<T> Res(s_);
         span<T> Res(WORK_.data(), s_);  // use WORK_ to avoid allocations
         for (int i = 0; i < s_; i++)
            for (int j = 0; j < s_; j++)
               Res[i] += last_Beta[j * s_ + i] * xs[i];

         // calculate norm of Res:
         T res = (T)0.0;
         for (int i = 0; i < s_; i++)
            res += Res[i] * Res[i];

         return sqrt(res);
      }

   private:
      int s_;    // block size
      int S_;    // block number of elements
      int N_;    // number of blocks
      int n_;    // nubmer of rows/columns
      int nev_;  // number of required eigenpairs

      std::vector<T> Alpha_;  // main diagonal blocks
      std::vector<T> Beta_;   // subdiagonal blocks

      std::vector<T> AB_;  // Lapack routine input array (band storage)

      std::vector<T> W_;      // Lapack routine resulting eigenvalues
      std::vector<T> Z_;      // Lapack routine resulting eigenvectors

      std::vector<T> Q_, WORK_; // Lapack routine auxiliary space
      std::vector<int> IWORK_;  // Lapack routine auxiliary space

      std::vector<int> IFAIL_;  // Lapack routine failed-to-coverge indexes

      // transformation of diagonal and subdiagonal blocks into band-storage:
      void pack()
      {
         int m = s_ + 1;

         AB_.resize(n_ * m);

         int l = 0;
         for (int k = 0 ; k < N_; k++)  // blocks
         {
            for (int j = 0; j < s_; j++)  // columns
            {
               for (int i = j; i < s_; i++)  // rows
                  AB_[l++] = Alpha_[S_ * k + j * s_ + i];
                  // block start index + column start index + row index
               
               for (int i = 0; i <= j; i++)
                  if (k < N_ - 1)
                     AB_[l++] = Beta_[S_ * k + j * s_ + i];
                     // block start index + column start index + row index
                  else
                     AB_[l++] = (T)0.0;
            }
         }
      }
};

}  // namespace ompilancz

#endif
