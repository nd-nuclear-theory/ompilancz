#ifndef OMPILANCZ_DETAIL_BLOCK_DIAGONAL_VECTOR_H
#define OMPILANCZ_DETAIL_BLOCK_DIAGONAL_VECTOR_H

#include <cassert>
#include <iomanip>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "diagonal_vector.h"
#include "mapping.h"

namespace ompilancz
{

// block of diagonal vectors split across diagonal processes:

template <typename T>
class block_diagonal_vector
{
   public:
      block_diagonal_vector(mapping& map)
         : map_(map)
      { }

      // copy semantics:

      block_diagonal_vector(const block_diagonal_vector&) = default;

      block_diagonal_vector& operator=(const block_diagonal_vector& other)
      {
         if (&other != this)
         {
            assert( &map_ == &other.map_ );  // both vectors must operate on same mapping object

            v_ = other.v_;
         }

         return *this;
      }

      // move semantics:

      block_diagonal_vector(block_diagonal_vector&& other) noexcept
         : map_(other.map_), v_(std::move(other.v_))
      { }

      block_diagonal_vector& operator=(block_diagonal_vector&& other) noexcept
      {
         if (&other != this)
         {
            assert( &map_ == &other.map_ );  // both vectors must operate on same mapping object

            v_ = std::move(other.v_);
         }

         return *this;
      }

      // properties:

      int size() const
      {
         return v_.size();
      }

      // vector accessors:

      diagonal_vector<T>& operator[](int i)
      {
         return v_[i];
      }

      const diagonal_vector<T>& operator[](int i) const
      {
         return v_[i];
      }

      // named member functions for easier call on pointers:
      diagonal_vector<T>& ith_vector(int i)
      {
         return v_[i];
      }

      const diagonal_vector<T>& ith_vector(int i) const
      {
         return v_[i];
      }

      // vector insrtion and removal:

      // add single with all zero elements
      void add_zero()
      {
         v_.emplace_back(map_);
      }

      // add multiple with all zero elements
      void add_multiple_zero(int n)
      {
         for (int i = 0; i < n; i++)
            v_.emplace_back(map_);
      }

      // add single with random elements
      void add_random(T a = (T)0.0, T b = (T)1.0)
      {
         v_.emplace_back(map_);
         v_.back().random(a, b);
      }

      void remove_last()
      {
         assert(v_.size() > 0);

         v_.pop_back();
      }

      // mathematical operations:

      T normalize_last()
      {
         return v_.back().normalize();
      }

      // orthogonalize last w.r.t. all preivous, and normalize
      std::pair<T, T> orthonormalize_last()
      {
         int n = v_.size();
         assert(n > 1);

         T alpha = v_[n - 1].orthogonalize(v_[n - 2]);

         for (int i = n - 3; i >= 0; i--)
            v_[n - 1].orthogonalize(v_[i]);

         T beta = v_[n - 1].normalize();

         return { alpha, beta };
      }

      // right multiplication by n x n column-major 2-D matrix:
      block_diagonal_vector<T> right_multiply_by_nxn(span<T> Y) const
      {
         uint64_t n = v_.size();

         assert( n > 0 );
         assert( Y.size() % n == 0 );

         int q = Y.size() / n;

         uint64_t m = v_[0].size();

         block_diagonal_vector X(map_);

         for (int k = 0; k < q; k++)
         {
            X.add_zero();

            span<T> y(Y.data() + k * n, n);

            for (uint64_t i = 0; i < m; i++)
               for (int j = 0; j < n; j++)
                  X[k][i] += v_[j][i] * y[j];
         }

         return X;
      }

      template <typename U = T>
      void store_to_files(const std::string& fileprefix, const std::string& extension, int num_width = 3)
      {
         for (int i = 0; i < v_.size(); i++)
         {
            std::ostringstream os;
            os << fileprefix;
            os << std::setw(num_width) << std::setfill('0') << i + 1;
            os << "." << extension;
            v_[i].template store_to_file<U>(os.str());
         }
      }

   private:
      mapping& map_;
      std::vector< diagonal_vector<T> > v_; 
};

}  // namespace ompilancz

#endif
