#ifndef LSU3SHELL_LIBRARIES_EIGENSOLVER_H
#define LSU3SHELL_LIBRARIES_EIGENSOLVER_H

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <memory>
#include <random>
#include <stdexcept>
#include <utility>
#include <vector>

#include <mpi.h>

// tri-diagonal eigensolver:

// LAPACK routines interface:
extern "C"
{
   extern void sstev_(char*, int*, float*, float*, float*, int*, float*, int*);
   extern void dstev_(char*, int*, double*, double*, double*, int*, double*, int*);

   extern void sstevx_(char*, char*, int*, float*, float*, float*, float*, int*, int*,
         float*, int*, float*, float*, int*, float*, int*, int*, int*);
   extern void dstevx_(char*, char*, int*, double*, double*, double*, double*, int*, int*,
         double*, int*, double*, double*, int*, double*, int*, int*, int*);

   extern float slamch_(char*);
   extern double dlamch_(char*);

   extern void ssbevx_(char*, char*, char*, int*, int*, float*, int*, float*, int*,
         float*, float*, int*, int*, float*, int*, float*, float*, int*,
         float*, int*, int*, int*);
   extern void dsbevx_(char*, char*, char*, int*, int*, double*, int*, double*, int*,
         double*, double*, int*, int*, double*, int*, double*, double*, int*,
         double*, int*, int*, int*);
}

// overloaded LAPACK wrappers:
void stev(char JOBZ, int N, float* D, float* E, float* Z, int LDZ, float* WORK, int& INFO)
{
   sstev_(&JOBZ, &N, D, E, Z, &LDZ, WORK, &INFO);
}

void stev(char JOBZ, int N, double* D, double* E, double* Z, int LDZ, double* WORK, int& INFO)
{
   dstev_(&JOBZ, &N, D, E, Z, &LDZ, WORK, &INFO);
}

template <typename T>
struct Lamch;

template <>
struct Lamch<float>
{
   static float lamch(char CMACH)
   {
      return slamch_(&CMACH);
   }
};

template <>
struct Lamch<double>
{
   static double lamch(char CMACH)
   {
      return dlamch_(&CMACH);
   }
};

void stevx(char JOBZ, char RANGE, int N, float* D, float* E, float VL, float VU, 
   int IL, int IU, float ABSTOL, int& M, float* W, float* Z, int LDZ, float* WORK, int* IWORK, int* IFAIL, int& INFO)
{
   sstevx_(&JOBZ, &RANGE, &N, D, E, &VL, &VU, &IL, &IU, &ABSTOL, &M, W, Z, &LDZ, WORK, IWORK, IFAIL, &INFO);
}

void stevx(char JOBZ, char RANGE, int N, double* D, double* E, double VL, double VU, 
   int IL, int IU, double ABSTOL, int& M, double* W, double* Z, int LDZ, double* WORK, int* IWORK, int* IFAIL, int& INFO)
{
   dstevx_(&JOBZ, &RANGE, &N, D, E, &VL, &VU, &IL, &IU, &ABSTOL, &M, W, Z, &LDZ, WORK, IWORK, IFAIL, &INFO);
}

void sbevx(char JOBZ, char RANGE, char UPLO, int N, int KD, float* AB, int LDAB, float* Q, int LDQ,
      float VL, float VU, int IL, int IU, float ABSTOL, int& M, float* W, float* Z, int LDZ,
      float* WORK, int* IWORK, int* IFAIL, int& INFO)
{
   ssbevx_(&JOBZ, &RANGE, &UPLO, &N, &KD, AB, &LDAB, Q, &LDQ, &VL, &VU, &IL, &IU, &ABSTOL,
         &M, W, Z, &LDZ, WORK, IWORK, IFAIL, &INFO);
}

void sbevx(char JOBZ, char RANGE, char UPLO, int N, int KD, double* AB, int LDAB, double* Q, int LDQ,
      double VL, double VU, int IL, int IU, double ABSTOL, int& M, double* W, double* Z, int LDZ,
      double* WORK, int* IWORK, int* IFAIL, int& INFO)
{
   dsbevx_(&JOBZ, &RANGE, &UPLO, &N, &KD, AB, &LDAB, Q, &LDQ, &VL, &VU, &IL, &IU, &ABSTOL,
         &M, W, Z, &LDZ, WORK, IWORK, IFAIL, &INFO);
}

template <typename T>
class tri_diagonal_eigensolver
{
   public:
      tri_diagonal_eigensolver(int n)
      {
         // preset sizes:
         alpha_.reserve(n);
         beta_.reserve(n);

         D_.reserve(n);
         E_.reserve(n);

         W_.resize(n);
         Z_.resize(n * n);

         WORK_.resize( std::max( std::max(0, 2 * n - 1), 5 * n ) );
         IWORK_.resize(5 * n);
         IFAIL_.resize(n);
      }

      void append_alphabeta(T alpha, T beta)
      {
         alpha_.push_back(alpha);
         beta_.push_back(beta);
      }

      void solve_all()
      {
         D_ = alpha_;
         E_ = beta_;

         int n = alpha_.size();

         Z_.resize(n * n);
         WORK_.resize( std::max(0, 2 * n - 1) );

         int INFO;
         stev('V', n, D_.data(), E_.data(), Z_.data(), n, WORK_.data(), INFO);

         if (INFO)
         {
            std::cerr << "Tri-diagonal eigensolver failed; ?stev INFO not null: " << INFO << std::endl;
            throw std::runtime_error("LAPACK ?STEV failed");
         }

         // results pointer:
         lambda_ = D_.data();
         x_ = Z_.data();
      }

      void solve_nev(int nev)
      {
         D_ = alpha_;
         E_ = beta_;

         int n = alpha_.size();

         W_.resize(n);
         Z_.resize(n * n);
         WORK_.resize(5 * n);
         IWORK_.resize(5 * n);
         IFAIL_.resize(n);

         T ABSTOL = Lamch<T>::lamch('S');
         int M, INFO;

         stevx('V', 'I', n, D_.data(), E_.data(), (T)0.0, (T)0.0, 1, std::min(n, nev), ABSTOL, M,
               W_.data(), Z_.data(), n, WORK_.data(), IWORK_.data(), IFAIL_.data(), INFO);

         if (INFO)
         {
            std::cerr << "Tri-diagonal eigensolver failed; ?stevx INFO not null: " << INFO << std::endl;
            throw std::runtime_error("LAPACK ?STEVX failed");
         }

         // results pointers:
         lambda_ = W_.data();
         x_ = Z_.data();
      }

      // i-th (0-indexed) eigenvalue:
      T lambda(int i) const { return lambda_[i]; }

      // i-th (0-indexed) eigenvector:
      const T* x(int i) const
      {
         int j = alpha_.size();
         return x_ + i * j;
      }

      // eigenvalue array:
      const T* lambda() const { return lambda_; }
      // eigenvectors array:
      const T* x() const { return x_; }

   private:
      std::vector<T> alpha_, beta_;
      std::vector<T> D_, E_, W_, Z_, WORK_;
      std::vector<int> IWORK_, IFAIL_;

      const T* lambda_;
      const T* x_;
};

template <typename T>
class block_tri_diagonal_eigensolver
{
   public:
      block_tri_diagonal_eigensolver(int n, int s) : s_(s)
      {
         // preset sizes:
         Alpha_.reserve(n);
         Beta_.reserve(n);
      }

      void append_AlphaBeta(std::vector<T>& Alpha, std::vector<T>& Beta)
      {
         Alpha_.push_back(std::move(Alpha));
         Beta_.push_back(std::move(Beta));
      }

      void solve_nev(int nev)
      {
         int n = Alpha_.size() * s_;
         int m = s_ + 1;

         // transform Alpha and Beta blocks to the LAPACK symmetric banded storage:

         AB_.resize(n * m);

         int i = 0;
         int k = 0;

         // before-the-last blocks:
         for ( ; i < Alpha_.size() - 1; i++)
         {
            for (int j = 0; j < s_; j++)
            {
               for (int a = j; a < s_; a++)
                  AB_[k++] = Alpha_[i][j * s_ + a];
               
               for (int b = 0; b <= j; b++)
                  AB_[k++] = Beta_[i][j * s_ + b];
            }
         }

         // last block:
         for (int j = 0; j < s_; j++)
         {
            for (int a = j; a < s_; a++)
               AB_[k++] = Alpha_[i][j * s_ + a];

            for (int b = 0; b <= j; b++)
               AB_[k++] = (T)0.0;
         }
         
         // setup array sizes:
         Q_.resize(n * n);
         W_.resize(n);
         Z_.resize(n * nev);
         WORK_.resize(7 * n);
         IWORK_.resize(5 * n);
         IFAIL_.resize(n);

         T ABSTOL = Lamch<T>::lamch('S');
         int M, INFO;

         sbevx('V', 'I', 'L', n, m - 1, AB_.data(), m, Q_.data(), n, (T)0.0, (T)0.0, 1, std::min(n, nev),
               ABSTOL, M, W_.data(), Z_.data(), n, WORK_.data(), IWORK_.data(), IFAIL_.data(), INFO);

         if (INFO)
         {
            std::cerr << "Band eigensolver failed; ?sbevx INFO not null: " << INFO << std::endl;
            throw std::runtime_error("LAPACK ?SBEVX failed");
         }

         // results pointers:
         lambda_ = W_.data();
         x_ = Z_.data();
      }

      // i-th (0-indexed) eigenvalue:
      T lambda(int i) const { return lambda_[i]; }

      // i-th (0-indexed) eigenvector:
      const T* x(int i) const
      {
         int j = Alpha_.size() * s_;
         return x_ + i * j;
      }

      // eigenvalue array:
      const T* lambda() const { return lambda_; }
      // eigenvectors array:
      const T* x() const { return x_; }

      // residual for i-th eigenpair:
      T res(int i) const
      {
         const std::vector<T>& B = Beta_.back();
         const T* x = x_ + Alpha_.size() * s_ * (i + 1) - s_;

         std::vector<T> temp(s_);

         for (int i = 0; i < s_; i++)
            for (int j = 0; j < s_; j++)
               temp[i] += B[j * s_ + i] * x[i];

         T res = (T)0.0;
         for (int i = 0; i < s_; i++)
            res += temp[i] * temp[i];
         res = sqrt(res);

         return res;
      }

   private:
      int s_;

      std::vector< std::vector<T> > Alpha_, Beta_;

      std::vector<T> AB_, Q_, W_, Z_, WORK_;
      std::vector<int> IWORK_, IFAIL_;

      const T* lambda_;
      const T* x_;
};

// MPI_Datatype helper for use in templates:

template <typename T>
struct Get_MPI_DataType;

template <>
struct Get_MPI_DataType<float>
{
   static MPI_Datatype get()
   {
      return MPI_FLOAT;
   }
};  

template <>
struct Get_MPI_DataType<double>
{
   static MPI_Datatype get()
   {
      return MPI_DOUBLE;
   }
};  

template <>
struct Get_MPI_DataType<long double>
{
   static MPI_Datatype get()
   {
      return MPI_LONG_DOUBLE;
   }
};  

// mapping of processes to matrix blocks:

class block_mapping
{
   public:
      block_mapping(int I, int J, int N, uint64_t n, uint64_t m, MPI_Comm allcomm = MPI_COMM_WORLD)
         : I_(I), J_(J), N_(N), n_(n), m_(m), root_(false)
      {
         MPI_Comm_dup(allcomm, &allcomm_);

         MPI_Comm_split(allcomm_, I, J, &hcomm_);
         MPI_Comm_split(allcomm_, J, J - I, &vcomm_);
         MPI_Comm_split(allcomm_, I == J, I, &dcomm_);

         diag_ = I == J;

         // first diagonal processor is root:
         if (diag_)
         {
            int rank;
            MPI_Comm_rank(dcomm_, &rank);
            root_ = (rank == 0);

            // diagonal blocks must be square
            assert(n_ == m_);
         }
      }

      ~block_mapping()
      {
         MPI_Comm_free(&dcomm_);
         MPI_Comm_free(&vcomm_);
         MPI_Comm_free(&hcomm_);
         MPI_Comm_free(&allcomm_);
      }

      uint64_t n() const { return n_; }
      uint64_t m() const { return m_; }

      bool root() const { return root_; }
      bool diag() const { return diag_; }

      MPI_Comm& allcomm() { return allcomm_; }
      MPI_Comm& hcomm() { return hcomm_; }
      MPI_Comm& vcomm() { return vcomm_; }
      MPI_Comm& dcomm() { return dcomm_; }

   private:
      int I_, J_, N_;   // process-block coordinates, number of diagonal blocks
      uint64_t n_, m_;  // process-block size (number of rows/columns)

      MPI_Comm allcomm_;                // all processes
      MPI_Comm hcomm_, vcomm_, dcomm_;  // horizontal, vertical, and diagonal communicators
      bool root_, diag_;
};

// vector split across diagonal processes:

template <typename T>
class diagonal_vector
{
   public:
      diagonal_vector(block_mapping& map)
         : map_(map), n_( map.n() ), data_( map.n() ), mdt_( Get_MPI_DataType<T>::get() )
      {
         // diagonal vectors are usable on diagonal processors only
         assert( map.diag() );

         // diagonal blocks must be square
         assert( map.n() == map.m() );
      }

      diagonal_vector& operator=(const diagonal_vector& other)
      {
         n_ = other.n_;
         data_ = other.data_;

         return *this;
      }

      T* data() { return data_.data(); }
      const T* data() const { return data_.data(); }

      T& operator[](uint64_t i) { return data_[i]; }
      const T& operator[](uint64_t i) const { return data_[i]; }

      uint64_t size() const
      {
         assert( n_ == data_.size() );
         return n_;
      }

      void random()
      {
         std::random_device rd;
         std::mt19937 eng(rd());
         std::uniform_real_distribution<T> dist((T)0.0, (T)1.0);

         for (auto & e : data_)
            e = dist(eng);
      }

      T dot_product(const diagonal_vector& other) const
      {
         assert(data_.size() == other.data_.size());

         T local_res = (T)0.0;
         
         for (uint64_t i = 0; i < n_; i++)
            local_res += data_[i] * other.data_[i];

         T global_res;
         MPI_Allreduce(&local_res, &global_res, 1, mdt_, MPI_SUM, map_.dcomm());

         return global_res;
      }

      T norm() const
      {
         T dot = dot_product(*this);
         return sqrt(dot);
      }

      T normalize()
      {
         T temp = norm();
         T mult = (T)1.0 / temp;
         for (auto & e : data_)
            e *= mult;

         return temp;
      }

      T orthogonalize(const diagonal_vector& other) 
      {
         T dot = dot_product(other);

         for (uint64_t i = 0; i < n_; i++)
            data_[i] -= dot * other.data_[i];

         return dot;
      }

      void add(const diagonal_vector& other)
      {
         for (int i = 0; i < n_; i++)
            data_[i] += other.data_[i];
      }

      void subtract(const diagonal_vector& other)
      {
         for (int i = 0; i < n_; i++)
            data_[i] -= other.data_[i];
      }

      void scaled_add(T alpha, const diagonal_vector& other)
      {
         for (int i = 0; i < n_; i++)
            data_[i] += alpha * other.data_[i];
      }

      void scaled_subtract(T alpha, const diagonal_vector& other)
      {
         for (int i = 0; i < n_; i++)
            data_[i] -= alpha * other.data_[i];
      }

      void scale(T alpha)
      {
         for (int i = 0; i < n_; i++)
            data_[i] *= alpha;
      }

   private:
      block_mapping& map_;
      uint64_t n_;
      std::vector<T> data_;
      MPI_Datatype mdt_;
};

// block of diagonal vectors:

template <typename T>
class block_diagonal_vector
{
   public:
      block_diagonal_vector(block_mapping& map, int s = 0)
         : map_(map)
      {
         v_.reserve(s);
      }

      void add(int n = 1)
      {
         for (int i = 0; i < n; i++)
            v_.emplace_back(map_);
      }

      void add_random()
      {
         v_.emplace_back(map_);
         v_.back().random();
      }

      void remove_last()
      {
         v_.pop_back();
      }

      T normalize_last()
      {
         return v_.back().normalize();
      }

      std::pair<T, T> orthonormalize_last()
      {
         auto n = v_.size();
         assert (n > 1);

         T alpha = v_[n - 1].orthogonalize(v_[n - 2]);

         for (int i = n - 3; i >= 0; i--)
            v_[n - 1].orthogonalize(v_[i]);

         T beta = v_[n - 1].normalize();

         return { alpha, beta };
      }

      diagonal_vector<T>& operator[](int i) { return v_[i]; }
      const diagonal_vector<T>& operator[](int i) const { return v_[i]; }

      block_diagonal_vector multiply_by_Y(const T* Y, int q)
      {
         int m = v_.size();
         int n = v_[0].size();

         block_diagonal_vector X(map_, q);

         for (int k = 0; k < q; k++)
         {
            X.add();

            const T* y = Y + k * m;

            for (uint64_t i = 0; i < n; i++)
               for (int j = 0; j < m; j++)
                  X[k][i] += v_[j][i] * y[j];
         }

         return X;
      }
      
      block_diagonal_vector multiply_by_Y(const T* Y)
      {
         return block_diagonal_vector(Y, v_.size());
      }

      void scaled_subtract(T* alphas, const block_diagonal_vector& other)
      {
         int m = v_.size();
         uint64_t n = v_[0].size();

         for (int j = 0; j < m; j++)
            for (uint64_t i = 0; i < n; i++)
               v_[j][i] -= alphas[j] * other.v_[j][i];
      }

      std::vector<T> norms()
      {
         int m = v_.size();

         std::vector<T> res;
         res.resize(m);

         for (int j = 0; j < m; j++)
            res[j] = v_[j].norm();

         return res;
      }

      std::vector<T> transposed_times_other(const block_diagonal_vector& other)
      {
         int s = v_.size();
         assert(s == other.v_.size());

         std::vector<T> res(s * s);

         for (int i = 0; i < s; i++)
            for (int j = 0; j < s; j++)
               res[s * j + i] = v_[i].dot_product(other.v_[j]);

         return res;
      }

      void block_scaled_subtract(const std::vector<T>& Alpha, const block_diagonal_vector& other)
      {
         int s = v_.size();
         assert(s == other.v_.size());
         assert(Alpha.size() = s * s);

         uint64_t n = v_[0].size();

         for (int k = 0; k < s; k++)
            for (uint64_t i = 0; i < n; i++)
               for(int j = 0; j < s; j++)
                  v_[k][i] -= Alpha[k * s + j] * other.v_[j][i];
      }

   private:
      block_mapping& map_;
      std::vector< diagonal_vector<T> > v_;
};

// matrix-vector multiplication:

template <typename T>
class single_vector_matvec
{
   public:
      single_vector_matvec(block_mapping& map)
         : map_(map), mdt_( Get_MPI_DataType<T>::get() )

      {
         // temporary arrays on non-diagonal processes:
         if (!map_.diag())
         {
            offdiag_x_.resize(map_.m());
            offdiag_xt_.resize(map_.n());
            offdiag_y_.resize(map_.n());
            offdiag_yt_.resize(map_.m());
         }
      }

      void diag_setup_xy( /* const */ diagonal_vector<T>* x, diagonal_vector<T>* y)
                          // not const because of MPI API flaw
      {
         assert(map_.diag());
         assert(x != nullptr);
         assert(y != nullptr);

         diag_x_ = x;
         diag_y_ = y;
      }

      void broadcast_x()
      {
         if (map_.diag())
         {
            MPI_Bcast(diag_x_->data(), map_.m(), mdt_, 0, map_.vcomm());
            MPI_Bcast(diag_x_->data(), map_.n(), mdt_, 0, map_.hcomm());
         }
         else
         {
            MPI_Bcast(offdiag_x_.data(), map_.m(), mdt_, 0, map_.vcomm());
            MPI_Bcast(offdiag_xt_.data(), map_.n(), mdt_, 0, map_.hcomm());
         }
      }

      template <typename U>
      void multiply(U&& single_vector_matvec_op)
      {
         if (map_.diag())
            single_vector_matvec_op(diag_x_->data(), nullptr, diag_y_->data(), nullptr);
         else
            single_vector_matvec_op(offdiag_x_.data(), offdiag_xt_.data(),
                  offdiag_y_.data(), offdiag_yt_.data());
      }

      void reduce_y()
      {
         if (map_.diag())
         {
            MPI_Reduce(MPI_IN_PLACE, diag_y_->data(), map_.n(), mdt_, MPI_SUM, 0, map_.hcomm());
            MPI_Reduce(MPI_IN_PLACE, diag_y_->data(), map_.m(), mdt_, MPI_SUM, 0, map_.vcomm());
         }
         else
         {
            MPI_Reduce(offdiag_y_.data(), nullptr, map_.n(), mdt_, MPI_SUM, 0, map_.hcomm());
            MPI_Reduce(offdiag_yt_.data(), nullptr, map_.m(), mdt_, MPI_SUM, 0, map_.vcomm());
         }
      }

   private:
      block_mapping& map_;
      MPI_Datatype mdt_;

      // data for diagonal processes:
      /* const */ diagonal_vector<T>* diag_x_; // not const because of MPI API flaw
      diagonal_vector<T>* diag_y_;

      // data for off-diagonal processes:
      std::vector<T> offdiag_x_, offdiag_xt_;
      std::vector<T> offdiag_y_, offdiag_yt_;
};

// matrix-block vector multiplication:

template <typename T>
class block_vector_matvec
{
   public:
      block_vector_matvec(block_mapping& map)
         : map_(map), mdt_( Get_MPI_DataType<T>::get() )

      { }

      void set_block_size(uint64_t s)
      {
         x_.resize(map_.m() * s);
         y_.resize(map_.n() * s);

         if (!map_.diag())
         {
            offdiag_xt_.resize(map_.n() * s);
            offdiag_yt_.resize(map_.m() * s);
         }

         s_ = s;
      }

      void diag_setup_xy( /* const */ block_diagonal_vector<T>* X, block_diagonal_vector<T>* Y)
                          // not const because of MPI API flaw
      {
         assert(map_.diag());
         assert(X != nullptr);
         assert(Y != nullptr);

         diag_X_ = X;
         diag_Y_ = Y;
      }

      void broadcast_X()
      {
         if (map_.diag())
         {
            // transformation from block-vector to linear array:
            for (uint64_t i = 0; i < map_.n(); i++)
               for (int j = 0; j < s_; j++)
                  x_[i * s_ + j] = (*diag_X_)[j][i];


            MPI_Bcast(x_.data(), s_ * map_.m(), mdt_, 0, map_.vcomm());
            MPI_Bcast(x_.data(), s_ * map_.n(), mdt_, 0, map_.hcomm());
         }
         else
         {
            MPI_Bcast(x_.data(), s_ * map_.m(), mdt_, 0, map_.vcomm());
            MPI_Bcast(offdiag_xt_.data(), s_ * map_.n(), mdt_, 0, map_.hcomm());
         }
      }

      template <typename U>
      void multiply(U&& block_vector_matvec_op)
      {
         if (map_.diag())
            block_vector_matvec_op(x_.data(), nullptr, y_.data(), nullptr, s_);
         else
            block_vector_matvec_op(x_.data(), offdiag_xt_.data(),
                  y_.data(), offdiag_yt_.data(), s_);
      }

      void reduce_Y()
      {
         if (map_.diag())
         {
            MPI_Reduce(MPI_IN_PLACE, y_.data(), s_ * map_.n(), mdt_, MPI_SUM, 0, map_.hcomm());
            MPI_Reduce(MPI_IN_PLACE, y_.data(), s_ * map_.m(), mdt_, MPI_SUM, 0, map_.vcomm());

            // transformation from linear array to block-vector:
            for (uint64_t i = 0; i < map_.n(); i++)
               for (int j = 0; j < s_; j++)
                  (*diag_Y_)[j][i] = y_[i * s_ + j];
         }
         else
         {
            MPI_Reduce(y_.data(), nullptr, s_ * map_.n(), mdt_, MPI_SUM, 0, map_.hcomm());
            MPI_Reduce(offdiag_yt_.data(), nullptr, s_ * map_.m(), mdt_, MPI_SUM, 0, map_.vcomm());
         }
      }

   private:
      block_mapping& map_;
      MPI_Datatype mdt_;

      // data for diagonal processes:
      /* const */ block_diagonal_vector<T>* diag_X_; // not const because of MPI API flaw
      block_diagonal_vector<T>* diag_Y_;

      uint64_t s_; // block size

      // data for all processes:
      std::vector<T> x_, y_;
      // additional data for off-diagonal processes:
      std::vector<T> offdiag_xt_, offdiag_yt_;
};

// Lanczos eigensolver:

template <typename T>
class Eigensolver
{
   public:
      Eigensolver(int I, int J, int N, uint64_t n, uint64_t m, T eps, int maxit, int nev)
         : map_(I, J, N, n, m), eps_(eps), maxit_(maxit), nev_(nev), smv_(map_), bmv_(map_)
      { }

#define MODIFIED
#ifndef MODIFIED
      template <typename U, typename V>
      void solve(U&& single_vector_matvec_op, V&& block_vector_matvec_op)
      {
         if (map_.root())
            std::cout << std::endl << "Starting eigensolver..." << std::endl;

         std::unique_ptr<std::ofstream> f_alphabeta;
         if (map_.root())
            f_alphabeta = std::make_unique<std::ofstream>("alphabeta.dat");

         // initial Lanczos vectors setup
         if (map_.diag())
         {
            v_ = std::make_unique< block_diagonal_vector<T> >(map_, maxit_ + 1);

            v_->add_random();
            v_->normalize_last();
//          v_->add();
//          if (map_.root()) (*v_)[0][0] = (T)1.0;
         }

         // setup tri-diagonal eigensolver on root:
         std::unique_ptr< tri_diagonal_eigensolver<T> > tde;
         if (map_.root())
            tde = std::make_unique< tri_diagonal_eigensolver<T> >(maxit_);

         bool stop = false;

         int j = 1;
         for ( ; j <= maxit_; j++)
         {
            // check convergence
            MPI_Bcast(&stop, 1, MPI_CXX_BOOL, 0, map_.allcomm());
            if (stop)
            {
               j--; // previous iteration was the last one
               break;
            }

            if (map_.diag())
               v_->add();

            // v_j <- A * v_(j-1)
            if (map_.diag())
                  smv_.diag_setup_xy(&(*v_)[j - 1], &(*v_)[j]);
            smv_.broadcast_x();
            smv_.multiply(single_vector_matvec_op);
            smv_.reduce_y();

            if (!map_.diag()) continue; // off-diagonal processes participate in matvec only

            T alpha, beta;
            std::tie(alpha, beta) = v_->orthonormalize_last();
            if (map_.root())
               tde->append_alphabeta(alpha, beta);

            if (map_.root())
               *f_alphabeta << alpha << "   " << beta << "\n";

            // evaluate eigenvalues and residuals:
            if (map_.root())
            {
               try 
               {
               // tde->solve_all();
                  tde->solve_nev(nev_);

                  // check convergence:
                  if (j >= nev_)
                  {
                     stop = true; // may stop

                     for (int i = 0; i < nev_; i++)
                     {
                        T res = beta * fabs( tde->x(i)[j - 1] );
                        if (res > eps_) stop = false;
                     }
                  }
                  
                  print_lambda_res(*tde, j, beta); // log
               }
               catch (...) { }
            }
         }

         // j = last iteration
         int m = j;

         // last Lanczos vector not needed any more
         if (map_.diag())
            v_->remove_last();

         MPI_Datatype mdt = Get_MPI_DataType<T>::get();

         // get final eigenpairs on diagonal processes:

         std::vector<T> Lambda;                         // final eigenvalues
         std::unique_ptr< block_diagonal_vector<T> > X; // final eigenvectors

         if (map_.diag())
         {
            // broadcast eigenvalues from root to diagonal processes:
            Lambda.resize(nev_);
            if (map_.root())
               std::copy(tde->lambda(), tde->lambda() + nev_, Lambda.begin());
            MPI_Bcast(Lambda.data(), nev_, mdt, 0, map_.dcomm());

            // broadcast projected eigenvectors to diagonal processes:
            std::vector<T> y(nev_ * m);
            if (map_.root())
               std::copy(tde->x(), tde->x() + nev_ * m, y.begin());
            MPI_Bcast(y.data(), nev_ * m, mdt, 0, map_.dcomm());

            // X <- Lanczos vectors * projected eigenvectors:
            X = std::make_unique< block_diagonal_vector<T> >( v_->multiply_by_Y( y.data(), nev_ ) );
         }

         // calculate residual norms:

// SINGLE-VECTOR-MULTIPLY SOLUTION:

         if (map_.root()) std::cout << std::endl;

         diagonal_vector<T> res(map_);

         for (int i = 0; i < nev_; i++)
         {
            // res_i <- A * x_i:
            if (map_.diag())
               smv_.diag_setup_xy(&(*X)[i], &res);
            smv_.broadcast_x();
            smv_.multiply(single_vector_matvec_op);
            smv_.reduce_y();

            if (!map_.diag()) continue;
            
            // res_i <- res_i - lambda_i * x_i:
            res.scaled_subtract(Lambda[i], (*X)[i]);

            // residual norm:
            T res_norm = res.norm();

            if (map_.root())
               std::cout << "Ev. " << std::right << std::setw(4) << i + 1 << " value = "
                  << std::right << std::setw(8) << std::setprecision(4) << std::fixed << Lambda[i]
                  << ", res = " << std::scientific << std::setprecision(3) << res_norm << std::endl;
         }

// BLOCK-VECTOR-MULTIPLY SOLUTION:

         if (map_.root()) std::cout << std::endl;

         block_diagonal_vector<T> RES(map_);
         RES.add(nev_);

         // RES <- A * X:
         bmv_.set_block_size(nev_);
         if (map_.diag())
            bmv_.diag_setup_xy(X.get(), &RES);
         bmv_.broadcast_X();
         bmv_.multiply(block_vector_matvec_op);
         bmv_.reduce_Y();

         if (map_.diag())
         {
            // RES <- RES - LAMBDA * X (element-wise):
            RES.scaled_subtract(Lambda.data(), *X);
            std::vector<T> RES_norms = RES.norms();

            if (map_.root())
               for (int i = 0; i < nev_; i++)
                  std::cout << "Ev. " << std::right << std::setw(4) << i + 1 << " value = "
                     << std::right << std::setw(8) << std::setprecision(4) << std::fixed << Lambda[i]
                     << ", res = " << std::scientific << std::setprecision(3) << RES_norms[i] << std::endl;
         }

         if (map_.root())
            std::cout << "...eigensolver finished." << std::endl;
      }
#else
      template <typename U, typename V>
      void solve(U&& single_vector_matvec_op, V&& block_vector_matvec_op)
      {
         if (map_.root())
            std::cout << std::endl << "Starting eigensolver..." << std::endl;

         std::unique_ptr<std::ofstream> f_alphabeta;
         if (map_.root())
            f_alphabeta = std::make_unique<std::ofstream>("alphabeta.dat");

         // initial Lanczos vectors setup

std::unique_ptr< block_diagonal_vector<T> > u_;

         if (map_.diag())
         {
            v_ = std::make_unique< block_diagonal_vector<T> >(map_, maxit_ + 1);

//          v_->add_random();
//          v_->normalize_last();
            v_->add();
            if (map_.root()) (*v_)[0][0] = (T)1.0;

u_ = std::make_unique< block_diagonal_vector<T> >(map_, maxit_ + 1);
u_->add();
u_->add();
         }

         // setup tri-diagonal eigensolver on root:
         std::unique_ptr< tri_diagonal_eigensolver<T> > tde;
         if (map_.root())
            tde = std::make_unique< tri_diagonal_eigensolver<T> >(maxit_);

         // u_2 <- A * v_(j-1)
         if (map_.diag())
               smv_.diag_setup_xy(&(*v_)[0], &(*u_)[1]);
         smv_.broadcast_x();
         smv_.multiply(single_vector_matvec_op);
         smv_.reduce_y();

         bool stop = false;

         int j = 1;
         for ( ; j <= maxit_; j++)
         {
            // check convergence
            MPI_Bcast(&stop, 1, MPI_CXX_BOOL, 0, map_.allcomm());
            if (stop)
            {
               j--; // previous iteration was the last one
               break;
            }

            if (map_.diag())
            {
               u_->add();
               v_->add();
            }

            // u_(j+2) <- A * u_(j+1) :
            if (map_.diag())
                  smv_.diag_setup_xy(&(*u_)[j], &(*u_)[j + 1]);
            smv_.broadcast_x();
            smv_.multiply(single_vector_matvec_op);
            smv_.reduce_y();

            if (!map_.diag()) continue; // off-diagonal processes participate in matvec only

            (*v_)[j] = (*u_)[j];

            auto & w = (*u_)[0];
            w = (*u_)[j]; 

            T alpha, beta;
            {
               alpha = (*v_)[j].orthogonalize((*v_)[j - 1]);
               w.scale(alpha);

               for (int i = j - 1; i > 0; i--)
               {
                  T gamma = (*v_)[j].orthogonalize((*v_)[i - 1]);
                  w.scaled_add(gamma, (*u_)[i]);
               }

               beta = (*v_)[j].normalize();
            }

            (*u_)[j + 1].subtract(w);
            (*u_)[j + 1].scale((T)1.0 / beta);
            
            if (map_.root())
               tde->append_alphabeta(alpha, beta);

            if (map_.root())
               *f_alphabeta << alpha << "   " << beta << "\n";

            // evaluate eigenvalues and residuals:
            if (map_.root())
            {
               try 
               {
               // tde->solve_all();
                  tde->solve_nev(nev_);

                  // check convergence:
                  if (j >= nev_)
                  {
                     stop = true; // may stop

                     for (int i = 0; i < nev_; i++)
                     {
                        T res = beta * fabs( tde->x(i)[j - 1] );
                        if (res > eps_) stop = false;
                     }
                  }
                  
                  print_lambda_res(*tde, j, beta); // log
               }
               catch (...) { }
            }
         }

         // j = last iteration
         int m = j;

         // last Lanczos vector not needed any more
         if (map_.diag())
            v_->remove_last();

         MPI_Datatype mdt = Get_MPI_DataType<T>::get();

         // get final eigenpairs on diagonal processes:

         std::vector<T> Lambda;                         // final eigenvalues
         std::unique_ptr< block_diagonal_vector<T> > X; // final eigenvectors

         if (map_.diag())
         {
            // broadcast eigenvalues from root to diagonal processes:
            Lambda.resize(nev_);
            if (map_.root())
               std::copy(tde->lambda(), tde->lambda() + nev_, Lambda.begin());
            MPI_Bcast(Lambda.data(), nev_, mdt, 0, map_.dcomm());

            // broadcast projected eigenvectors to diagonal processes:
            std::vector<T> y(nev_ * m);
            if (map_.root())
               std::copy(tde->x(), tde->x() + nev_ * m, y.begin());
            MPI_Bcast(y.data(), nev_ * m, mdt, 0, map_.dcomm());

            // X <- Lanczos vectors * projected eigenvectors:
            X = std::make_unique< block_diagonal_vector<T> >( v_->multiply_by_Y( y.data(), nev_ ) );
         }

         // calculate residual norms:

// SINGLE-VECTOR-MULTIPLY SOLUTION:

         if (map_.root()) std::cout << std::endl;

         diagonal_vector<T> res(map_);

         for (int i = 0; i < nev_; i++)
         {
            // res_i <- A * x_i:
            if (map_.diag())
               smv_.diag_setup_xy(&(*X)[i], &res);
            smv_.broadcast_x();
            smv_.multiply(single_vector_matvec_op);
            smv_.reduce_y();

            if (!map_.diag()) continue;
            
            // res_i <- res_i - lambda_i * x_i:
            res.scaled_subtract(Lambda[i], (*X)[i]);

            // residual norm:
            T res_norm = res.norm();

            if (map_.root())
               std::cout << "Ev. " << std::right << std::setw(4) << i + 1 << " value = "
                  << std::right << std::setw(8) << std::setprecision(4) << std::fixed << Lambda[i]
                  << ", res = " << std::scientific << std::setprecision(3) << res_norm << std::endl;
         }

         if (map_.root())
            std::cout << "...eigensolver finished." << std::endl;
      }
#endif

      template <typename U>
      void solve(U&& block_vector_matvec_op)
      {
         auto single_vector_matvec_op
            = [&block_vector_matvec_op]
              (const T* x, const T* xt, T* y, T* yt)
              {
                 block_vector_matvec_op(x, xt, y, yt, 1);
              };

         solve(single_vector_matvec_op, block_vector_matvec_op);
      }

      template <typename U>
      void block_solve(U&& block_vector_matvec_op, int s /* block size */ )
      {
         if (map_.root())
            std::cout << std::endl << "Starting block eigensolver..." << std::endl;

         std::unique_ptr<std::ofstream> f_AlphaBeta;
         if (map_.root())
            f_AlphaBeta = std::make_unique<std::ofstream>("AlphaBeta.dat");

         // initial Lanczos block-vectors setup
         if (map_.diag())
         {
            bv_ = std::make_unique< std::vector< block_diagonal_vector<T> >>();
            bv_->emplace_back(map_, s);

            // add first vector:
            bv_->back().add_random();
            bv_->back().normalize_last();

            for (int k = 1; k < s; k++)
            {
               bv_->back().add_random();
               bv_->back().orthonormalize_last();
            }
         }

         // setup banded eigensolver on root:
         std::unique_ptr< block_tri_diagonal_eigensolver<T> > btde;
         if (map_.root())
            btde = std::make_unique< block_tri_diagonal_eigensolver<T> >(maxit_, s);

         bmv_.set_block_size(s);

         bool stop = false;

         int j = 1;
         for ( ; j <= maxit_; j++)
         {
            // check convergence
            MPI_Bcast(&stop, 1, MPI_CXX_BOOL, 0, map_.allcomm());
            if (stop)
            {
               j--; // previous iteration was the last one
               break;
            }

            if (map_.diag())
            {
               bv_->emplace_back(map_, s);
               bv_->back().add(s);
            }

            // V_j <- A * V_(j-1)
            if (map_.diag())
               bmv_.diag_setup_xy(&bv_->operator[](j - 1), &bv_->operator[](j));
            bmv_.broadcast_X();
            bmv_.multiply(block_vector_matvec_op);
            bmv_.reduce_Y();

            if (!map_.diag()) continue; // off-diagonal processes participate in matvec only

            // diagonal block A_j:
            std::vector<T> Alpha = bv_->operator[](j - 1).transposed_times_other(bv_->operator[](j));
            bv_->operator[](j).block_scaled_subtract(Alpha, bv_->operator[](j - 1));

            // full block orthogonalization:
            std::vector<T> temp;
            for (int i = j - 2; i >= 0; i--)
            {
               temp = bv_->operator[](i).transposed_times_other(bv_->operator[](j));
               bv_->operator[](j).block_scaled_subtract(temp, bv_->operator[](i));
            }

            block_diagonal_vector<T>& V = bv_->operator[](j);

            // QR-factorization:
            std::vector<T> Beta(s * s);

            for (int i = 0; i < s; i++)
            {
               Beta[i * s + i] = V[i].normalize();
               
               for (int j = i + 1; j < s; j++)
               {
                  T dot = V[i].dot_product(V[j]);
                  Beta[j * s + i] = dot;
                  V[j].scaled_subtract(dot, V[i]);
               }
            }

            if (map_.root())
               *f_AlphaBeta << Alpha[0] << "   " << Beta[0] << "\n";
            
            // append Alpha and Beta blocks to the banded matrix:
            if (map_.root())
               btde->append_AlphaBeta(Alpha, Beta);

            // evaluate eigenvalues:
            if (map_.root())
            {
               try
               {
                  btde->solve_nev(nev_);

                  // check convergence:
                  if (j * s >= nev_) 
                  {
                     stop = true; // may stop

                     for (int i = 0; i < nev_; i++)
                        if (btde->res(i) > eps_) stop = false;
                  }

                  print_block_lambda(*btde, j, s); // log
               }
               catch (...) { }
            }
         }

         // j = last (block) iteration
         int m = j * s;

         MPI_Datatype mdt = Get_MPI_DataType<T>::get();

         // get final eigenpairs on diagonal processes:

         std::vector<T> Lambda;                         // final eigenvalues
         std::unique_ptr< block_diagonal_vector<T> > X; // final eigenvectors

         if (map_.diag())
         {
            // broadcast eigenvalues from root to diagonal processes:
            Lambda.resize(nev_);
            if (map_.root())
               std::copy(btde->lambda(), btde->lambda() + nev_, Lambda.begin());
            MPI_Bcast(Lambda.data(), nev_, mdt, 0, map_.dcomm());

            // broadcast projected eigenvectors to diagonal processes:
            std::vector<T> y(nev_ * m);
            if (map_.root())
               std::copy(btde->x(), btde->x() + nev_ * m, y.begin());
            MPI_Bcast(y.data(), nev_ * m, mdt, 0, map_.dcomm());

            // gather first nev Lanczos vectors:
            block_diagonal_vector<T> temp(map_, m);

            for (int k = 0; k < j; k++)
               for (int l = 0; l < s; l++)
               {
                  temp.add();
                  temp[k * s + l] = bv_->operator[](k)[l];
               }

            // X <- Lanczos vectors * projected eigenvectors:
            X = std::make_unique< block_diagonal_vector<T> >( temp.multiply_by_Y( y.data(), nev_ ) );
         }

         // calculate residual norms:

         if (map_.root()) std::cout << std::endl;

         block_diagonal_vector<T> RES(map_);
         RES.add(nev_);

         // RES <- A * X:
         bmv_.set_block_size(nev_);
         if (map_.diag())
            bmv_.diag_setup_xy(X.get(), &RES);
         bmv_.broadcast_X();
         bmv_.multiply(block_vector_matvec_op);
         bmv_.reduce_Y();

         if (map_.diag())
         {
            // RES <- RES - LAMBDA * X (element-wise):
            RES.scaled_subtract(Lambda.data(), *X);
            std::vector<T> RES_norms = RES.norms();

            if (map_.root())
               for (int i = 0; i < nev_; i++)
                  std::cout << "Ev. " << std::right << std::setw(4) << i + 1 << " value = "
                     << std::right << std::setw(8) << std::setprecision(4) << std::fixed << Lambda[i]
                     << ", res = " << std::scientific << std::setprecision(3) << RES_norms[i] << std::endl;
         }

         if (map_.root())
            std::cout << "...block eigensolver finished." << std::endl;
      }

   private:
      block_mapping map_;
      T eps_;
      int maxit_, nev_;
      single_vector_matvec<T> smv_; 
      block_vector_matvec<T> bmv_;

      std::unique_ptr< block_diagonal_vector<T> > v_;                // Lanczos vectors
      std::unique_ptr< std::vector< block_diagonal_vector<T> >> bv_; // Lanczos block-vectors

      // iteration log for 1st, 2nd, and nev-th eigenpairs:
      void print_lambda_res(const tri_diagonal_eigensolver<T>& tde, int j, T beta)
      {
         std::cout << "It. " << std::right << std::setw(4) << j << " Ev.: ";

         print_lambda_res_ith(tde, j, 0, beta);
         if (j > 1)
            print_lambda_res_ith(tde, j, 1, beta); 
         if ((j >= nev_) && (nev_ > 2))
            print_lambda_res_ith(tde, j, nev_ - 1, beta);

         std::cout << std::endl;
      }

      // iteration log for ith eigenpair:
      void print_lambda_res_ith(const tri_diagonal_eigensolver<T>& tde, int j, int i, T beta)
      {
         // residual norm for ith eigenpair:
         T res = beta * fabs( tde.x(i)[j - 1] );

         std::cout << std::right << std::setw(8) << std::setprecision(4) << std::fixed << tde.lambda(i)
            << " (" << std::scientific << std::setprecision(3) << res << ")   ";
      }

      void print_block_lambda(const block_tri_diagonal_eigensolver<T>& btde, int j, int s)
      {
         std::cout << "It. " << std::right << std::setw(4) << j << " Ev.: ";

         print_lambda_res_ith(btde, j, 0);
         if (j * s > 1)
            print_lambda_res_ith(btde, j, 1); 
         if ((j * s >= nev_) && (nev_ > 2))
            print_lambda_res_ith(btde, j, nev_ - 1);

         std::cout << std::endl;
      }

      void print_lambda_res_ith(const block_tri_diagonal_eigensolver<T>& btde, int j, int i)
      {
         std::cout << std::right << std::setw(8) << std::setprecision(4) << std::fixed << btde.lambda(i)
            << " (" << std::scientific << std::setprecision(3) << btde.res(i) << ")   ";
      }
};

#endif
