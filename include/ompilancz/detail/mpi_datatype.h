#ifndef OMPILANCZ_DETAIL_MPI_DATATYPE_H
#define OMPILANCZ_DETAIL_MPI_DATATYPE_H

#include <mpi.h>

namespace ompilancz
{

// helper to get MPI_Datatype instances in generic code:

template <typename T>
struct mpi_datatype;

template <>
struct mpi_datatype<float>
{
   static MPI_Datatype get()
   {
      return MPI_FLOAT;
   }
};

template <>
struct mpi_datatype<double>
{
   static MPI_Datatype get()
   {
      return MPI_DOUBLE;
   }
};

}  // namespace ompilancz

#endif
